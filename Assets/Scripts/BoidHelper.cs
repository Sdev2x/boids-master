﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class BoidHelper {

    public static readonly Vector3[] directions;

   
    const int numViewDirections = 300;
    static BoidHelper () {
        directions = new Vector3[BoidHelper.numViewDirections];

        float goldenRatio = (1 + Mathf.Sqrt (5)) / 2;       // golden ratio 1.618 
        float angleIncrement = Mathf.PI * 2 * goldenRatio;  // 2 * pi * angle > for circle

        for (int i = 0; i < numViewDirections; i++) {
            float t = (float) i / numViewDirections;       //0 to 1 distance (Turn fraction)
            float phi = Mathf.Acos (1 - 2 * t);           // inclination : acos(1 - 2 u)
            float theta = angleIncrement * i;             // azimuth : angle each iteration turn some fraction 

            float x = Mathf.Sin (phi) * Mathf.Cos (theta);
            float y = Mathf.Sin (phi) * Mathf.Sin (theta);
            float z = Mathf.Cos (phi);
            directions[i] = new Vector3 (x, y, z);
        }
    }
}








    // View Angle 
    // Steer Away      > 
    // Same Direction  > rotation axis
    // Towards Center  > position .
    // UML

// inclination = phi
// azimuth = theta

// from numpy import pi, cos, sin, arccos, arange
// import mpl_toolkits.mplot3d
// import matplotlib.pyplot as pp

// num_pts = 1000
// indices = arange(0, num_pts, dtype=float) + 0.5

// phi = arccos(1 - 2*indices/num_pts)
// theta = pi * (1 + 5**0.5) * indices

// x, y, z = cos(theta) * sin(phi), sin(theta) * sin(phi), cos(phi);

// pp.figure().add_subplot(111, projection='3d').scatter(x, y, z);
// pp.show()