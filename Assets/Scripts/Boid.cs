﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




// Every Specific Nanobot Properties and Behaviour

public class Boid : MonoBehaviour {

    //fps check .
    public float deltaTime;

    // HideInInspector > Don't show in Inspecter Menu


    // Initializing weight values, later referenced and changed with UI Manager .
    int alignWeight;
    int cohesionWeight;
    int seperateWeight;


 
    Vector3 velocity; // velocity vector will be update each frame (Magnitude + Direction)
    Vector3 acceleration;

    [HideInInspector]
    public Vector3 avgFlockHeading; // For Alignment
    [HideInInspector]
    public Vector3 avgAvoidanceHeading; // For Separation
    [HideInInspector]
    public Vector3 centreOfFlockmates; // For Cohesion
    [HideInInspector]
    public int numPerceivedFlockmates;
    
    // Material material;

    // Target/Tumor Coordinates
    Transform target;
    // Settings Class Variable
    BoidSettings settings;

    // Position variable for bot
    [HideInInspector]
    public Vector3 position;
    [HideInInspector]  
    public Vector3 forward; // forward vector variable for bot (z axis in position)


    // cache variable for transform component of bot (Position , Rotation, Scale)
    Transform cachedTransform;

    void Awake () {
        // material = transform.GetComponentInChildren<MeshRenderer> ().material;

        cachedTransform = transform; //Storing transform variable in cacheTransform.
    }

    // This Function Called In Boid Manager, (To access values from boid manager variables)
    public void Initialize (BoidSettings settings, Transform target, UIController ui) {

        // Getting target and settings variables form boid manager and saving into Boid
        this.target = target;
        this.settings = settings;

        // Getting position and forward vector from same boid and saving in position , forward
        // position and forward are the main variables which will be changed
        position = cachedTransform.position;
        forward = cachedTransform.forward;

        // startSpeed = average of min and max speed
        float startSpeed = (settings.minSpeed + settings.maxSpeed) / 2; //Step Length ?

        // Initialize Velocity (with starting speed * (Initial z axis position of boiud))
        velocity = transform.forward * startSpeed;


        //Saving UI values in variables
        alignWeight = ui.selectedAlignment;
        cohesionWeight = ui.selectedCohesion;
        seperateWeight = ui.selectedSeparation;
    }


    

    // public void SetColour (Color col) {
    //     if (material != null) {
    //         material.color = col;
    //     }
    // }




    // Performing Calculation and Changing Position And Forward at every frame
    // Function Called In Boids Manager
    public void UpdateBoid () {
        // Initialize Acceleration = Zero
        Vector3 acceleration = Vector3.zero;

        // If (number of neighbours) are not equal to zero (Calculated inside Boid Manager)
        if (numPerceivedFlockmates != 0) {
            //Debug.Log (numPerceivedFlockmates); (If Total 55 then preceived 54) 

            // Updating val of Center Of All Boids .
            centreOfFlockmates /= numPerceivedFlockmates; 
            // Calulating Offset For Cohesion (Simple Formula)
            Vector3 offsetToFlockmatesCentre = (centreOfFlockmates - position);

            // Three Rules
            // For Alignment it checks the avg direction of other nanobots (avgFlockHeading) 
            //and change its direction (Note:To get the direction , we need the forward vector of transform)
            // avgFlockHeading is calculated in boids manager
            var alignmentForce = SteerTowards (avgFlockHeading) * alignWeight;
        
            // For Cohesion Calculate average center of all other bots, then calculate direction
            // towards center of flock from its current location
            var cohesionForce = SteerTowards (offsetToFlockmatesCentre) * cohesionWeight;
            
            // For Separation we calculate inverse of (average distance from current bot to other bots)
            var seperationForce = SteerTowards (avgAvoidanceHeading) * seperateWeight;


            
            // Updating values in acceleration
            acceleration += alignmentForce;
            acceleration += cohesionForce;
            acceleration += seperationForce;
        }




        // If going to collide with an obstacle
        // calc which direction to move then calc how much force
        // then move
        if (IsHeadingForCollision ()) {

            // Which Direction To Move (Got from Ray Casting - Fibonnaci Sphere .)
            Vector3 collisionAvoidDir = ObstacleRays ();

            // Move Towards Avaialbe Direction (Smoothly)
            Vector3 collisionAvoidForce = SteerTowards (collisionAvoidDir) * settings.avoidCollisionWeight;

            // Addition in coordinates value
            acceleration += collisionAvoidForce;
        }

        velocity += acceleration* Time.deltaTime; //* Time.deltaTime (with respect to time smooth movement) (Not Slow Movement)

        // Magnitude Of Velocity Is Called Speed
        float speed = velocity.magnitude;
        // Debug.Log(speed); // 7.88

        // Direction calculated by ratio of velocity and speed
        Vector3 dir = velocity / speed;
        // Debug.Log(dir); // (0.0,0.1,-1.0)

        //Mathf.Clamp >> to restrict speed inside the range of min and max speed
        speed = Mathf.Clamp (speed, settings.minSpeed, settings.maxSpeed);
        
        // -----------------

        // Finalized velocity (product of direction and normalized speed)
        velocity = dir * speed;

        // These two (transform.postion, transform.forward) are responsible for
        // Boid movement
        cachedTransform.position += velocity * Time.deltaTime; // position > (x,y,z)
        // * Time.deltaTime; because of smooth movement with respect to time (Not Sudden Movement)
        cachedTransform.forward = dir; // z-axis


        // Updated Forward Vector and position
        position = cachedTransform.position;
        forward = dir; //forward .. ?   Transform.forward change z axis also its rotation
        // Debug.Log(position);
        // Debug.Log(forward);
        // Debug.Log("Restart");




        // if (reachedTarget()){
        //     Debug.Log("Reached Target");
        // }

    }

    // If Reached Target Return True
    public bool reachedTarget(){
        RaycastHit hit;

        if (Physics.SphereCast (position, settings.boundsRadius, forward, out hit, settings.collisionAvoidDst, settings.targetMask)) {
            return true;
        } else { }
        return false; 
    }

    // Function continuosly checking for colission is gonna happen or not (return true or false)
    bool IsHeadingForCollision () {

        // RaycastHit is a Structure used to get information back from a raycast.
        RaycastHit hit;
        //(origin, length, forward, out, maxDistance, layer)

        if (Physics.SphereCast (position, settings.boundsRadius, forward, out hit, settings.collisionAvoidDst, settings.obstacleMask)) {
            return true;
        } else { }
        return false;
    }


    // Casting multiple rays and checking for collision if its find the ray is not hitting something in specified distance
    // it will move towards it
    // Return Direction
    Vector3 ObstacleRays () {
        // List Of Vectors Rays
        Vector3[] rayDirections = BoidHelper.directions;   //BoidHelper > golden ratio spiral

        // Loop 300 Times , cast 300 Rays One By One
        for (int i = 0; i < rayDirections.Length; i++) {
            
            //Transforms direction x, y, z from local space to world space. To Convert Transform Object To Vector Object
            Vector3 dir = cachedTransform.TransformDirection(rayDirections[i]); //?
            // Vector3 dir = rayDirections[i]; //?

            // If Didn't TransformDirection(rayDirections[i]); then remain at same place and moves
            
            // Ray is Infinite Line Starts from a position and goes in a specific direction
            // Direction is always a normalized vector
            Ray ray = new Ray (position, dir);

            
            //To Visualize Ray
            Debug.DrawRay(position, dir, Color.green, 5.0f);

          

            // bool True when the sphere sweep intersects any collider
            if (!Physics.SphereCast (ray, settings.boundsRadius, settings.collisionAvoidDst, settings.obstacleMask)) {
                return dir;
            }

            
            // SphereCast(Vector3 origin,  Vector3 direction, float radius,  float maxDistance, layerMask)
            // LayerMask is used for selectively ignore colliders
            // If didn't collide then return the direction which where you move
            // If Going going to collide didn't return that direction, loop continue
        }
        

        // Returned forward if no direction extracted (all rays collided with obstacle) ()
        return forward;
    }

   
    // // Extra Function For Showing Ray
    void OnDrawGizmosSelected()
    {
        // TransformDirection > relative to object
        // Draws a 5 unit long red line in front of the object
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(Vector3.forward) * 5;
        Gizmos.DrawRay(transform.position, direction);
    }

    
    Vector3 SteerTowards (Vector3 vector) {

        // Limiting the magnitude of a vector to the specified maxSpeed
        // Slow down if you get here
        // Preventing Agent going off the map
        Vector3 steering = (vector.normalized * settings.maxSpeed) - velocity; //Steering
        // Normalizing a vector
        // The result is a vector with the same direction, but with a magnitude of 1.
        
        // Returns a copy of vector with its magnitude clamped to maxLength.
        // Limit the magnitude of a vector to the specified max
        return Vector3.ClampMagnitude (steering, settings.maxSteerForce);  
    }

}


//desired_velocity = normalize (position - target) * max_speed
//     steering = desired_velocity - velocity
// If a character continues to seek, it will eventually pass through the target, and then turn back to approach again. This produces motion a bit like a moth buzzing around a light bulb. Contrast this with the description of arrival below.


//https://www.reddit.com/r/Unity3D/comments/gla2ic/how_do_i_render_the_inside_of_a_3d_object/
//https://stackoverflow.com/questions/42253311/unity-how-to-make-material-double-sided
//Cancer color D91A46
//Carcenoma ate downwards due to gastric
// Nanorobot , Boid , Boids
//Group Best, Personal Best

// Research About Golden Ratio
// https://www.math.temple.edu/~reich/Fib/fibo.html
// http://www.cs.toronto.edu/~dt/siggraph97-course/cwr87/
// https://stackoverflow.com/questions/9600801/evenly-distributing-n-points-on-a-sphere/44164075#44164075
// https://math.libretexts.org/Courses/College_of_the_Canyons/Math_100%3A_Liberal_Arts_Mathematics_(Saburo_Matsumoto)/07%3A_Mathematics_and_the_Arts/7.02%3A_The_Golden_Ratio_and_Fibonacci_Sequence


// The step size is equal to speed times frame time.

// https://docs.unity3d.com/2018.3/Documentation/ScriptReference/Vector3.RotateTowards.html

// transform.forward is shorthand for transform.rotation*Vector3.forward;

// Steer Towards
// https://calculator.academy/normalize-vector-calculator/
// https://forum.unity.com/threads/how-this-method-work-vector3-clampmagnitude.514937/
// https://docs.unity3d.com/2018.3/Documentation/Manual/DirectionDistanceFromOneObjectToAnother.html

// IMP
// https://github.com/robu3/swarming-unity/blob/master/Assets/DroneBehavior.cs
// https://www.cc.gatech.edu/~surban6/2019fa-gameAI/lectures/2019_09_16_steering-presented%20in%20class.pdf
// http://www.red3d.com/cwr/steer/gdc99/










// Duplicate
// // If going to collide with an obstacle (If true)
//     // calc which direction to move then calc how much force
//     // then move
//     if (IsHeadingForCollision ()) {

//         // Which Direction To Move If gonna Collide
//         Vector3 collisionAvoidDir = ObstacleRays (); //?
//         // Debug.Log(collisionAvoidDir); (0.1,-0.2,-1.0) ?

//         Vector3 collisionAvoidForce = SteerTowards (collisionAvoidDir) * settings.avoidCollisionWeight; //?
//         // Debug.Log(collisionAvoidForce); (2.3,25.9,-0.1) (19.4,-25.3,-0.1) ?

//         // Addition in acceleration value
//         acceleration += collisionAvoidForce;
//     }




//Vector3 SteerTowards (Vector3 vector) {

    //     // . Normalized = Returns this vector with a magnitude of 1
    //     // Magnitude = Returns the length of this vector (Read Only). The length of the vector is square root of (x*x+y*y+z*z).


    //     // Limiting the magnitude of a vector to the specified maxSpeed
    //     // Slow down if you get here
    //     // Preventing Agent going off the map
    //     //desired_velocity = normalize (position - target) * max_speed
    //     //steering = desired_velocity - velocity
    //     Vector3 desiredv = (vector.normalized * settings.maxSpeed) - velocity; //Steering
    //     // Velocity is vector
    //     // Debug.Log(vector);
    //     // Debug.Log(vector.normalized);
    //     // Debug.Log(velocity);
    //     // Debug.Log("Normalized");
        

    //     // Vector3 targetDirection = target - transform.position;
	// 	// float targetDistance = targetDirection.magnitude;
    //     // steer = targetDirection - rigidbody.velocity;


    //     Debug.Log("Input Vector 3");
    //     Debug.Log(vector);
    //     Debug.Log("Output Vector 3");
    //     Debug.Log(Vector3.ClampMagnitude (v, settings.maxSteerForce));
        
    //     // Normalizing a vector:
    //     // This process involves dividing a vector by its magnitude. 
    //     // The result is a vector with the same direction, but with a magnitude of 1.

    //     //Returns a copy of vector with its magnitude clamped to maxLength.
    //     // Limit the magnitude of a vector to the specified max
    //     return Vector3.ClampMagnitude (v, settings.maxSteerForce);  


    //     // What is Clamp (9,9,9) < (3,3 ,3)
    //     // Manipulating Vector Magnitude ?

    // }

// Flow Diagram
// Start > Ray Casting > Flocking > Obstacle Avoidance > Tumor Detections



// In Update Boid
// //FPS CHECK CODE
        // deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
        // float fps = 1.0f / deltaTime;
        // Debug.Log(fps); //60-70fps

        // Initialize Acceleration = Zero
        //   Vector3 acceleration = Vector3.zero;

        // Logic For Target Hard Coded Coordinates 
        // if (target != null) {
        //     Vector3 offsetToTarget = (target.position - position);
        //     acceleration = SteerTowards (offsetToTarget) * settings.targetWeight;
        // }
