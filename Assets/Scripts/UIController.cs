using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public Text numberOfBotsText;
    public Text numberOfAlignmentText;
    public Text numberOfCohesionText;
    public Text numberOfSeparationText;


    public GameObject uiScreen;
    public GameObject thespawner;
    public GameObject themanager;



    [HideInInspector]
    public int selectedNumOfBots;
    [HideInInspector]
    public int selectedAlignment;
    [HideInInspector]
    public int selectedCohesion;
    [HideInInspector]
    public int selectedSeparation;



    // Start is called before the first frame update
    void Start()
    {
        // numberOfBotsText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void botsTextUpdate(float value){
        numberOfBotsText.text = Mathf.RoundToInt(value * 100) + "";
        //Debug.Log(int.Parse(numberOfBotsText.text));

        selectedNumOfBots = int.Parse(numberOfBotsText.text);


    }


    public void startSimulation(){

        if (selectedNumOfBots > 1 && selectedAlignment >= 1 && selectedSeparation >= 1 && selectedCohesion >= 1){
            Debug.Log("Started");
            Debug.Log(selectedNumOfBots);
            uiScreen.SetActive(false);
            thespawner.SetActive(true);
            themanager.SetActive(true);


        }
        else{
            Debug.Log("Please select value greater than 1");
        }
        

    }


    public void botsAlignmentUpdate(float value){
        numberOfAlignmentText.text = Mathf.RoundToInt(value * 10) + "";
        selectedAlignment = int.Parse(numberOfAlignmentText.text);


    }
    public void botsCohesionUpdate(float value){
        numberOfCohesionText.text = Mathf.RoundToInt(value * 10) + "";
        selectedCohesion = int.Parse(numberOfCohesionText.text);


    }
    public void botsSeparationUpdate(float value){
        numberOfSeparationText.text = Mathf.RoundToInt(value * 10) + "";
        selectedSeparation = int.Parse(numberOfSeparationText.text);


    }
}
