using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]

// Used By Boids.cs

public class BoidSettings : ScriptableObject {
    // Settings

    
    // Speed Of Boids between Max And Min Speed
    public float minSpeed = 2; // 5
    public float maxSpeed = 8; // 8 prev 5 .

    // ???
    public float perceptionRadius = 2.5f; //2.5f
    public float maxSteerForce = 3;
    public float targetWeight = 1; // 2

    // If Avoidance Radius Increased (More Separated)
    public float avoidanceRadius = 1; // 1

    

    // Increase Value for More Alignment (Weired Behaviour at Val 10) , Decrease Value for less Alignment
    public float alignWeight = 1; // 2 , 2  (was set to 1)

    // If Increase Value > More Towards Center, If Decrease > Wider Range
    public float cohesionWeight = 1; // 1

    // If Increase Value > More Separate, If Decrease > More Closed
    public float seperateWeight = 1; //2.5

    

    [Header ("Collisions")]
    public LayerMask obstacleMask;

    public LayerMask targetMask;

    // If changing value greater or less than one (Weird Behaviour) Don't know why used
    // public float boundsRadius = .27f; // 0.27
    public float boundsRadius = .27f; //0.27

    // Not Totally Understood ?? (Weird Behaviour)
    // How much collision will be avoided with Obstacles
    public float avoidCollisionWeight = 10; // 20 //8.28

    // Distance at which it starts to avoid collision with obstacle
    public float collisionAvoidDst = 0.0005f; // 5

}