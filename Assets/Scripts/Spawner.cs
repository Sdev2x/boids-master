﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Spawner : MonoBehaviour {

    public UIController ui;
    

    public Boid prefab;
    public int spawnCount = 10; //350
    public float spawnRadius = 10; //2.37



    // For Just Displaying Spawn Area
    public enum GizmoType { Never, SelectedOnly, Always }
    public Color colour;
    public GizmoType showSpawnRegion;
    // public bool startSimulate;


    


    void Awake () {

        

        Debug.Log(ui.selectedNumOfBots);
        spawnCount = ui.selectedNumOfBots;

        // Run loop
        for (int i = 0; i < spawnCount; i++) {

            // Defining Position for a Bot to Spawn
            // Random.insideUnitSphere = // random point inside or on a sphere with radius 1.0
            Vector3 pos = transform.position + Random.insideUnitSphere * spawnRadius;

            // Spawn Prefab
            Boid boid = Instantiate (prefab);
            
            // giving position to the bot
            boid.transform.position = pos;

            
        }
    

        
    }

    // Not Know The Usage - Used Inside Awake
    // boid.transform.forward = Random.insideUnitSphere;
    // boid.SetColour (colour);


    // For Just Displaying Spawn Area (3 Functions)
    private void OnDrawGizmos () {
        if (showSpawnRegion == GizmoType.Always) {
            DrawGizmos ();
        }
    }

    void OnDrawGizmosSelected () {
        if (showSpawnRegion == GizmoType.SelectedOnly) {
            DrawGizmos ();
        }
    }

    void DrawGizmos () {

        Gizmos.color = new Color (colour.r, colour.g, colour.b, 0.3f);
        Gizmos.DrawSphere (transform.position, spawnRadius);
    }

}